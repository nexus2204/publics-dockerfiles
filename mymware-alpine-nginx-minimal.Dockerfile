FROM alpine:3.9.6

MAINTAINER Manuel Barrios <manuel.barrios@mymware.com>

RUN apk add --update nginx && rm -rf /var/cache/apk/*
RUN mkdir -p /tmp/nginx/client-body

COPY public/index.html /usr/share/nginx/html/
COPY conf/nginx.conf /etc/nginx/nginx.conf

EXPOSE 80 443

CMD ["nginx", "-g", "daemon off;"]


